  <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
        <img src="{{url($admin->admin_image)}}" alt="admin" style="width: 48px; border-style: groove; border-radius: 50%;"> 
          <!-- <i class="fas fa-laugh-wink"></i> -->
        </div>
        <div class="sidebar-brand-text mx-3" style="font-size: 13px;">{{$admin->admin_name}}</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{route('admin-index')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('notification1')}}">
          <i class="fas fa-fw fa-paper-plane"></i>
          <span>Send Notification</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-bullseye"></i>
          <span>Settings</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{route('edit-admin',[$admin->id])}}">Edit Profile</a>
            <a class="collapse-item" href="{{route('delivery_timing')}}">Delivery Timing</a>
            <a class="collapse-item" href="{{route('edit_sms_api')}}">SMS API Key</a>
            <a class="collapse-item" href="{{route('edit_fcm_api')}}">FCM Server Key</a>
            <a class="collapse-item" href="{{route('edit-logo')}}">Edit App Logo</a>
            <a class="collapse-item" href="{{route('currency')}}">Edit Currency</a>
            <a class="collapse-item" href="{{route('paymentvia')}}">Payment Mode</a>
            
          </div>
        </div>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="{{route('city')}}">
          <i class="fas fa-fw fa-university"></i>
          <span>City</span></a>
      </li>
  
      <li class="nav-item">
        <a class="nav-link" href="{{route('cityadmin')}}">
          <i class="fas fa-fw fa-user"></i>
          <span>City Admin</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('alluser')}}">
          <i class="fas fa-fw fa-users"></i>
          <span>User Management</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('all_plan')}}">
          <i class="fas fa-fw fa-file"></i>
          <span>Membership Plans</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="{{route('deal')}}">
          <i class="fas fa-fw fa-credit-card"></i>
          <span>First Recharge Offer</span></a>
      </li>
       
       <li class="nav-item">
        <a class="nav-link" href="{{route('wallet_credits')}}">
          <i class="fas fa-fw fa-circle"></i>
          <span>Give Free Credits</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="{{route('plan')}}">
          <i class="fas fa-fw fa-table"></i>
          <span>Subscription Plans</span></a>
      </li>
    
       <li class="nav-item">
        <a class="nav-link" href="{{route('complain')}}">
          <i class="fas fa-fw fa-list"></i>
          <span>Complain manager</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="{{route('cancel_reason')}}">
          <i class="fas fa-fw fa-window-close"></i>
          <span>Cancelling Reasons</span></a>
      </li>
      
      </li>
        <li class="nav-item">
        <a class="nav-link" href="{{route('faq')}}">
          <i class="fas fa-fw fa-cubes"></i>
          <span>FAQs</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
